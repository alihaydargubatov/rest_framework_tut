from django.conf.urls import url, include
from django.contrib import admin
from snippets.serializers import SnippetSerializer, UserSerializer
from snippets import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include('snippets.urls')),
]
